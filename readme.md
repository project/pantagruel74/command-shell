<h1>pantagruel74/command-shell</h1>

<h2>Requirements</h2>
- PHP >= 7.4
- php-ext-mbstring

<h2>Description</h2>
Command shell for executing multi-line sh commands sets. Using tmp files.

<h2>Install</h2>
- composer require "pantagruel74/command-shell"

<h2>Using</h2>
- Use Pantagruel74\CommandShell\Command object
- Initialize it, with commands string and directory path for tmp files (absolute path). Default creates tmp files in object directory.
- Call executeCommand() method for save all commands execution output into variable.
- Commands should be contained in one string variable, divided "\n" symbol 

<h2>Example</h2>
<div><code>$command = new Command("echo 111\necho abs", __DIR__);</code></div>
<div><code>$result = $command->executeCommand(); 
    <span style="color:#777;">//$result contains "111\nabs\n"</span></code></div>

<h2>License</h2>
- MIT

<h2>Contacts</h2>
- Anatoly Starodubtsev
- Tostar74@mail.ru
