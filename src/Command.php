<?php

namespace Pantagruel74\CommandShell;

use Pantagruel74\CommandShell\exceptions\FileWriteErrorException;
use Ramsey\Uuid\Uuid;

class Command
{
    protected string $command = '';
    protected string $workDirectory = '';

    /**
     * @param string $command
     * @param string $workDirectory
     */
    public function __construct(string $command, string $workDirectory = '')
    {
        $this->command = $this->purifyCommand($command);
        if(empty($workDirectory)) {
            $workDirectory = __DIR__;
        }
        if(in_array(mb_substr($workDirectory, -1), ["/","\\"])) { //normalize path
            $workDirectory = mb_substr($workDirectory, 0, mb_strlen($workDirectory) - 1);
        }
        $this->workDirectory = $workDirectory;
    }

    /**
     * @return string
     */
    public function getCommandText(): string
    {
        return $this->command;
    }

    /**
     * @return string
     * @throws FileWriteErrorException
     */
    public function executeCommand(): string
    {
        $runtimeUuid = Uuid::uuid4()->toString();
        $ds = DIRECTORY_SEPARATOR;
        $scriptName = $this->workDirectory . $ds . $runtimeUuid . ".sh";
        $scriptName = str_replace("\\", "/", $scriptName); //protect from error at windows
        return $this->executeInTempFile($scriptName, $this->command);
    }

    /**
     * @param string $scriptFileName
     * @param string $commands
     * @return string
     * @throws FileWriteErrorException
     */
    protected function executeInTempFile(string $scriptFileName, string $commands): string
    {
        if(mb_substr($scriptFileName, -3) !== '.sh') {
            $scriptFileName .= '.sh';
        }
        $logFileName = mb_substr($scriptFileName, 0, mb_strlen($scriptFileName) - 3) . '.log';
        $commands = $this->addLogOutIntoCommand($commands, $logFileName);
        $this->writeScriptSafety($scriptFileName, $commands);
        $advResult = shell_exec("chmod +x " . $scriptFileName);
        $advResult .= shell_exec($scriptFileName);
        $log = file_get_contents($logFileName);
        unlink($scriptFileName);
        unlink($logFileName);
        if(empty(str_replace(["\n", " "], "", $log))) {
            return $advResult;
        }
        return $log;
    }

    /**
     * @param string $command
     * @param $logFileName
     * @return string
     */
    protected function addLogOutIntoCommand(string $command, $logFileName): string
    {
        $commandLinesArray = explode("\n", $command);
        foreach ($commandLinesArray as $key => &$commandLine)
        {
            if($key === 0) {
                $commandLine .= ' 1>' . $logFileName;
            } else {
                $commandLine .= ' 1>>' . $logFileName;
            }
            $commandLine .= ' 2>>' . $logFileName;
        }
        return implode("\n", $commandLinesArray);
    }

    /**
     * @param string $command
     * @return string
     */
    protected function purifyCommand(string $command): string
    {
        return str_replace("\n\r", "\n", $command);
    }

    /**
     * @param string $fullFileName
     * @param string $commands
     * @return void
     * @throws FileWriteErrorException
     */
    protected function writeScriptSafety(string $fullFileName, string $commands): void
    {
        $ds = DIRECTORY_SEPARATOR;
        if(file_put_contents($fullFileName, "#!/bin/bash\n" . $commands) == 0) {
            unlink($fullFileName);
            throw new FileWriteErrorException("Can't write file " . $fullFileName . " : 0 bytes was wrote");
        }
    }

}