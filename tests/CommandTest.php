<?php
use Pantagruel74\CommandShell\Command;

class CommandTest extends \PHPUnit\Framework\TestCase
{
    public function testCommon()
    {
        $command = new Command("echo 111\necho abs", __DIR__);
        $result = $command->executeCommand();
        $this->assertEquals("111\nabs\n", $result);
        $this->assertEquals($command->getCommandText(), "echo 111\necho abs");
    }
}